# Rich text generator for Flutter
This generator uses a modified version of ProseMirror format for presenting rich-text in applications based on Flutter.

***

## Supported types of nodes of the rich text

- simple text with styles (bold, italic, underlined, strikethrough)
- text references with styles
- title and headings
- line breaks
- ordered and unordered lists
- code blocks
- quote blocks
- images with captions 
- attached files
- formulas
- tables

***

## Package structure
```
tapcourse_rich_text
├── lib
│   ├── assets
│   │   └── config.json   - configuration for the rich text generator (text styles for some nodes)
│   ├── src
│   │   ├── entities      - definition of nodes
│   │   ├── generator     - rich text generator (Visitor pattern)
│   │   ├── parser        - parser of input format
│   │   ├── printer       - rich text printer (Visitor pattern)
│   │   ├── main.dart     - definition of the main class
│   │   └── utils.dart    - some utilities
│   └── rich_text.dart    - exposed API
├── build.yaml            - configuration for the code generator 
└── pubspec.yaml          - dependency declaration
```

***

## JSON format of nodes

Use this format to declare rich text and send to the generator.

#### Root of the rich text

```json
{
    "type": "doc",
    "content": [
        // top level nodes (in a column)
    ]
}
```

#### Paragraph

```json
{
    "type": "paragraph",
    "content": [
        // paragraph nodes (in a row)
    ]
}
```

#### Line break

```json
{
    "type": "hard_break"
}
```

#### Simple text with styles (including references)

```json
{
    "type": "text",
    "marks": [
        { "type": "bold" },      
        { "type": "italic" },    
        { "type": "strike" },
        { "type": "underline" },
        { "type": "link", "attrs": { "href": "<url>" }},
        { "type": "code" } // can be applied separately from other styles
    ],
    "text": "...some text..."
}
```

#### Title
Rich text can have only one title as the first top level node.

```json
{
    "type": "title",
    "content": [
        // title nodes (in a row)
    ]
}
```

#### Headings
Rich text can have several headings.

```json
{
    "type": "heading",
    "attrs": { "level": 1 }, // level may be 1, 2 or 3
    "content": [
        // heading nodes (in a row)
    ]
}
```

#### Ordered and unordered lists

```json
{
    "type": "ordered_list", // or "bullet_list" for unordered list
    "content": [
        {
            "type": "list_item",
            "content": [
                // item nodes (in a column)
            ]
        },
        ...
    ]
}
```

#### Code blocks

```json
{
    "type": "code_block",
    "content": [
        // block nodes (in a column)
    ]
}
```

#### Quotes

```json
{
    "type": "blockquote",
    "content": [
        // quote nodes (in a column)
    ]
}
```

#### Images with captions 

```json
{
    "type": "image",
    "attrs": {
        "file": {
            "url": "<url>",
            "name": "file name",
            "size": 1024 // in bytes
        },
        "caption": "...some caption..." // may be omitted
    }
}
```

#### Attached files

```json
{
    "type": "attachment",
    "attrs": {
        "file": {
            "url": "<url>",
            "name": "file name",
            "size": 1024 // in bytes
        }
    }
}
```

#### Formulas

```json
{
    "type": "latex",
    "attrs": {
        "value": "...latex formula..."
    }
}
```

#### Tables

```json
{
    "type": "table",
    "content": [
        {
            "type": "table_row",
            "content": [
                {
                    "type": "table_cell",
                    "attrs": {
                        "colspan": 1,
                        "rowspan": 1
                    },
                    "content": [
                        // cell nodes (in a column)
                    ]
                },
                ...
            ]
        },
        ...
    ]
}
```

***

## Widget generator
The rich text generator is implemented using the ***Visitor*** pattern.

![Scheme of the rich text generator](pictures/generator_scheme.png)

***

## Usage

```dart
// Initializes rich text generator
await TapCourseRichTextGenerator.initialize();
// Defines input data
Map<String, dynamic> json = {...};
// Generates rich text widget
var widget = await TapCourseRichTextGenerator.generate(json);
```