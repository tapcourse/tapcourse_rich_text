import 'dart:ui';
import 'package:json_annotation/json_annotation.dart';

part 'config.g.dart';

@JsonSerializable()
class NodeStyle {
  final double textSize;
  final String font;
  final Color color;

  @JsonKey(name: 'color')
  final String colorValue;

  NodeStyle(this.textSize, this.font, this.colorValue) : color = Color(int.parse(colorValue));

  factory NodeStyle.fromJson(Map<String, dynamic> json) => _$NodeStyleFromJson(json);

  @override
  String toString() => '[text size: $textSize, font: $font]';
}

@JsonSerializable()
class RichTextConfig {
  final NodeStyle simple;
  final NodeStyle codeBlock;

  @JsonKey(name: 'h1')
  final NodeStyle headline1;

  @JsonKey(name: 'h2')
  final NodeStyle headline2;

  @JsonKey(name: 'h3')
  final NodeStyle headline3;

  RichTextConfig({
    required this.simple,
    required this.headline1,
    required this.headline2,
    required this.headline3,
    required this.codeBlock,
  });

  factory RichTextConfig.fromJson(Map<String, dynamic> json) {
    var config = _$RichTextConfigFromJson(json);
    return config;
  }

  @override
  String toString() {
    return 'Rich-text configuration:\n'
        '- simple     : $simple\n'
        '- h1         : $headline1\n'
        '- h2         : $headline2\n'
        '- h3         : $headline3\n'
        '- code block : $codeBlock';
  }
}
