import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_layout_grid/flutter_layout_grid.dart';
import 'package:flutter_tex_js/flutter_tex_js.dart';
import 'package:rich_text/src/generator/config.dart';
import 'package:rich_text/src/utils.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../rich_text.dart';
import '../entities/entities.dart';

abstract class Widgetable {
  Widget toWidget(WidgetGenerator generator);
}

const Widget _emptyWidget = const SizedBox();

class WidgetGenerator {
  final RichTextConfig config;
  NodeStyle _currentNodeStyle;
  bool _currentNodeIsCodeBlock = false;
  bool _isTopTable = true;

  WidgetGenerator(this.config) : _currentNodeStyle = config.simple;

  Widget generateText(TextNode node) {
    List<TextDecoration> textDecors = [];
    if (node.styles & TextNode.STRIKE != 0) textDecors.add(TextDecoration.lineThrough);
    if (node.styles & TextNode.UNDERLINE != 0) textDecors.add(TextDecoration.underline);
    Widget textWidget = Text(
      node.text,
      softWrap: true,
      style: TextStyle(
        fontSize: _currentNodeStyle.textSize,
        fontFamily: _currentNodeStyle.font,
        color: (node.url != null) ? Colors.blue : _currentNodeStyle.color,
        fontWeight: (node.styles & TextNode.BOLD != 0) ? FontWeight.bold : FontWeight.normal,
        fontStyle: (node.styles & TextNode.ITALIC != 0 || _currentNodeIsCodeBlock) ? FontStyle.italic : FontStyle.normal,
        decoration: (textDecors.isNotEmpty) ? TextDecoration.combine(textDecors) : TextDecoration.none,
      ),
    );
    if (node.url != null) {
      return InkWell(
        child: textWidget,
        onTap: () async {
          if (await canLaunch(node.url!)) {
            await launch(node.url!);
          }
        },
      );
    }
    if (node.styles & TextNode.CODE != 0) {
      return Container(
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(
          color: Colors.grey.shade300,
          borderRadius: BorderRadius.circular(8),
        ),
        child: textWidget,
      );
    }
    return textWidget;
  }

  Widget generateHardBreak(HardBreakNode node) {
    return Container(height: 8);
  }

  Widget generateParagraph(ParagraphNode node) {
    return _generateRowOfNodes(node.children);
  }

  Widget generateTitle(TitleNode node) {
    var oldStyle = _currentNodeStyle;
    _currentNodeStyle = config.headline1;
    var widget = Container(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: _generateRowOfNodes(node.children),
    );
    _currentNodeStyle = oldStyle;
    return widget;
  }

  Widget generateHeading(HeadingNode node) {
    var oldStyle = _currentNodeStyle;
    _currentNodeStyle = (node.level == 1)
        ? config.headline1
        : (node.level == 2)
            ? config.headline2
            : config.headline3;
    var widget = Container(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: _generateRowOfNodes(node.children),
    );
    _currentNodeStyle = oldStyle;
    return widget;
  }

  Widget generateListItem(ListItemNode node) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          (node.order == 0) ? '⚫' : '${node.order}.',
          style: TextStyle(fontSize: config.simple.textSize),
        ),
        SizedBox(width: 8),
        Expanded(
          child: _generateColumnOfNodes(node.children),
        ),
      ],
    );
  }

  Widget generateList(ListNode node) {
    return _generateColumnOfNodes(node.children, gapSize: 8);
  }

  Widget generateCodeBlock(CodeBlockNode node) {
    var oldStyle = _currentNodeStyle;
    _currentNodeStyle = config.codeBlock;
    var widget = Container(
      padding: EdgeInsets.all(16),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.circular(8),
      ),
      child: _generateColumnOfNodes(node.children),
    );
    _currentNodeStyle = oldStyle;
    return widget;
  }

  Widget generateQuoteBlock(QuoteBlockNode node) {
    var oldStyle = _currentNodeStyle;
    _currentNodeStyle = config.simple;
    _currentNodeIsCodeBlock = true;
    var widget = Container(
      width: double.infinity,
      padding: EdgeInsets.only(top: 8, bottom: 8, left: 16, right: 8),
      decoration: BoxDecoration(
        color: Colors.grey[300],
        border: Border(
          left: BorderSide(
            color: Colors.grey.shade500,
            width: 4,
          ),
        ),
      ),
      child: _generateColumnOfNodes(node.children),
    );
    _currentNodeStyle = oldStyle;
    _currentNodeIsCodeBlock = false;
    return widget;
  }

  Widget generateImage(ImageNode node) {
    if (node.url.isEmpty) return _emptyWidget;
    return Container(
      alignment: Alignment.center,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.network(node.url),
          if (node.caption.isNotEmpty) generateText(TextNode(node.caption)),
        ],
      ),
    );
  }

  Widget generateAttachment(AttachmentNode node) {
    if (node.url.isEmpty) return _emptyWidget;
    return Container(
      padding: const EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: Colors.grey.shade200,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Row(
        children: [
          // File icon
          Container(
            padding: const EdgeInsets.all(8),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8),
            ),
            child: Icon(Icons.description_outlined, size: 24, color: Colors.blue),
          ),
          const SizedBox(width: 16),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // File name
                Text(
                  node.fileName,
                  maxLines: 1,
                  overflow: TextOverflow.fade,
                  style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                const SizedBox(height: 4),
                // File size
                Text(
                  formatSizeInBytes(node.sizeInBytes),
                  maxLines: 1,
                  overflow: TextOverflow.fade,
                  style: const TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Colors.grey),
                ),
              ],
            ),
          ),
          const SizedBox(width: 16),
          // Download icon
          GestureDetector(
            onTap: () => downloadFile(node.url),
            behavior: HitTestBehavior.translucent,
            child: Icon(Icons.file_download, size: 32, color: Colors.grey),
          ),
        ],
      ),
    );
  }

  Widget generateFormula(FormulaNode node) {
    return TexImage(
      node.texCode,
      displayMode: false,
      fontSize: _currentNodeStyle.textSize,
      error: (_, __) => _emptyWidget,
    );
  }

  Widget generateTable(TableNode node) {
    var isTop = _isTopTable;
    _isTopTable = false;

    var table = Container(
      padding: const EdgeInsets.all(0.5),
      color: Colors.grey.shade500,
      child: LayoutGrid(
        columnSizes: List.filled(node.columnAmount, auto),
        rowSizes: List.filled(node.rowAmount, auto),
        children: node.cells.map((cell) => cell.toWidget(this)).toList(),
      ),
    );

    _isTopTable = isTop;

    return (_isTopTable) ? SingleChildScrollView(scrollDirection: Axis.horizontal, child: table) : table;
  }

  Widget generateTableRow(TableRowNode node) {
    return _emptyWidget;
  }

  Widget generateTableCell(TableCellNode node) {
    return GridPlacement(
      columnStart: node.startColumn,
      rowStart: node.startRow,
      columnSpan: node.columnSpan,
      rowSpan: node.rowSpan,
      child: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
          border: Border.all(width: 0.5, color: Colors.grey.shade500),
          color: Colors.white,
        ),
        padding: const EdgeInsets.all(8),
        child: _generateColumnOfNodes(node.children, gapSize: 8),
      ),
    );
  }

  Widget generateIntermediate(IntermediateNode node) {
    return _generateColumnOfNodes(node.children, gapSize: 16);
  }

  Widget _generateColumnOfNodes(List<Node> nodes, {double gapSize = 0}) {
    if (nodes.isEmpty) return _emptyWidget;
    List<Widget> widgets = [];
    for (int i = 0; i < nodes.length; i++) {
      var w = nodes[i].toWidget(this);
      if (w != _emptyWidget) {
        widgets.add(w);
        if (gapSize > 0 && i != nodes.length - 1) widgets.add(SizedBox(height: gapSize));
      }
    }

    if (widgets.length == 1) {
      return widgets.first;
    }

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: widgets,
    );
  }

  Widget _generateRowOfNodes(List<Node> nodes) {
    if (nodes.isEmpty) return _emptyWidget;
    return Wrap(
      crossAxisAlignment: WrapCrossAlignment.center,
      children: [
        for (var node in nodes) node.toWidget(this),
      ],
    );
  }
}
