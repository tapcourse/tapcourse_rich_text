// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'config.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NodeStyle _$NodeStyleFromJson(Map<String, dynamic> json) {
  return NodeStyle(
    (json['text_size'] as num).toDouble(),
    json['font'] as String,
    json['color'] as String,
  );
}

RichTextConfig _$RichTextConfigFromJson(Map<String, dynamic> json) {
  return RichTextConfig(
    simple: NodeStyle.fromJson(json['simple'] as Map<String, dynamic>),
    headline1: NodeStyle.fromJson(json['h1'] as Map<String, dynamic>),
    headline2: NodeStyle.fromJson(json['h2'] as Map<String, dynamic>),
    headline3: NodeStyle.fromJson(json['h3'] as Map<String, dynamic>),
    codeBlock: NodeStyle.fromJson(json['code_block'] as Map<String, dynamic>),
  );
}
