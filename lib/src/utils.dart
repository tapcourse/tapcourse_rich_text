import 'dart:io';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

Future<bool> requestStoragePermission() async {
  if (Platform.isAndroid) {
    var status = await Permission.storage.status;
    if (!status.isGranted) {
      var newStatus = await Permission.storage.request();
      if (!newStatus.isGranted) {
        return false;
      }
    }
  }
  return true;
}

Future<String> findLocalPath() async {
  final directory =
      Platform.isAndroid ? (await getExternalStorageDirectory() ?? await getApplicationDocumentsDirectory()) : await getApplicationDocumentsDirectory();
  return directory.path;
}

Future<String> findDownloadsPath() async {
  var dirPath = (await findLocalPath()) + Platform.pathSeparator + 'Download';
  final dir = Directory(dirPath);
  bool hasExisted = await dir.exists();
  if (!hasExisted) {
    await dir.create();
  }
  return dirPath;
}

Future<void> downloadFile(String url) async {
  await requestStoragePermission();
  await FlutterDownloader.enqueue(
    url: url,
    savedDir: await findDownloadsPath(),
    showNotification: true, // show download progress in status bar (for Android)
    openFileFromNotification: true, // click on notification to open downloaded file (for Android)
  );
}

String formatSizeInBytes(int size) {
  if (size < 1024) {
    return '$size B';
  } else if (size < 1024 * 1024) {
    var s = (size / 1024 * 100).toInt() / 100;
    return '$s Kb';
  } else if (size < 1024 * 1024 * 1024) {
    var s = (size / 1024 / 1024 * 100).toInt() / 100;
    return '$s Mb';
  } else {
    var s = (size / 1024 / 1024 / 1024 * 100).toInt() / 100;
    return '$s Gb';
  }
}
