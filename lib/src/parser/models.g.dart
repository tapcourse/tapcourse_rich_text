// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JsonNode _$JsonNodeFromJson(Map<String, dynamic> json) {
  return JsonNode(
    type: json['type'] as String,
    content: (json['content'] as List<dynamic>?)
        ?.map((e) => JsonNode.fromJson(e as Map<String, dynamic>))
        .toList(),
    marks: (json['marks'] as List<dynamic>?)
        ?.map((e) => Mark.fromJson(e as Map<String, dynamic>))
        .toList(),
    text: json['text'] as String?,
    attrs: json['attrs'],
  );
}

Mark _$MarkFromJson(Map<String, dynamic> json) {
  return Mark(
    json['type'] as String,
    json['attrs'] as Map<String, dynamic>?,
  );
}

FileAttribute _$FileAttributeFromJson(Map<String, dynamic> json) {
  return FileAttribute(
    json['url'] as String,
    json['name'] as String,
    json['size'] as int,
  );
}

AttachmentAttrs _$AttachmentAttrsFromJson(Map<String, dynamic> json) {
  return AttachmentAttrs(
    json['file'] == null
        ? null
        : FileAttribute.fromJson(json['file'] as Map<String, dynamic>),
  );
}

ImageAttrs _$ImageAttrsFromJson(Map<String, dynamic> json) {
  return ImageAttrs(
    json['file'] == null
        ? null
        : FileAttribute.fromJson(json['file'] as Map<String, dynamic>),
    json['caption'] as String,
  );
}

HeadingAttrs _$HeadingAttrsFromJson(Map<String, dynamic> json) {
  return HeadingAttrs(
    json['level'] as int,
  );
}

FormulaAttrs _$FormulaAttrsFromJson(Map<String, dynamic> json) {
  return FormulaAttrs(
    json['value'] as String,
  );
}

ReferenceAttrs _$ReferenceAttrsFromJson(Map<String, dynamic> json) {
  return ReferenceAttrs(
    json['href'] as String,
  );
}

TableCellAttrs _$TableCellAttrsFromJson(Map<String, dynamic> json) {
  return TableCellAttrs(
    json['colspan'] as int,
    json['rowspan'] as int,
  );
}
