import 'package:json_annotation/json_annotation.dart';

part 'models.g.dart';

@JsonSerializable()
class JsonNode {
  final String type;
  final List<JsonNode>? content;
  final dynamic attrs;
  final List<Mark>? marks;
  final String? text;

  JsonNode({
    required this.type,
    this.content,
    this.marks,
    this.text,
    this.attrs,
  });

  factory JsonNode.fromJson(Map<String, dynamic> json) => _$JsonNodeFromJson(json);

  @override
  String toString() => _toPrettyString(0);

  String _toPrettyString(int depth) =>
      '${' ' * depth}type: $type\n' +
      (marks != null ? '${' ' * depth}attrs: $marks\n' : '') +
      (attrs != null ? '${' ' * depth}attrs: ${attrs.runtimeType}\n' : '') +
      (text != null ? '${' ' * depth}text: $text\n' : '') +
      (content != null ? '${content!.map((e) => e._toPrettyString(depth + 4)).join()}' : '');
}

@JsonSerializable()
class Mark {
  final String type;
  final Map<String, dynamic>? attrs;

  Mark(this.type, this.attrs);

  factory Mark.fromJson(Map<String, dynamic> json) => _$MarkFromJson(json);

  @override
  String toString() => '$type';
}

@JsonSerializable()
class FileAttribute {
  final String url;
  final String name;
  final int size;

  FileAttribute(this.url, this.name, this.size);

  factory FileAttribute.fromJson(Map<String, dynamic> json) => _$FileAttributeFromJson(json);
}

@JsonSerializable()
class AttachmentAttrs {
  final FileAttribute? file;

  AttachmentAttrs(this.file);

  factory AttachmentAttrs.fromJson(Map<String, dynamic> json) => _$AttachmentAttrsFromJson(json);
}

@JsonSerializable()
class ImageAttrs {
  final FileAttribute? file;
  final String caption;

  ImageAttrs(this.file, this.caption);

  factory ImageAttrs.fromJson(Map<String, dynamic> json) => _$ImageAttrsFromJson(json);
}

@JsonSerializable()
class HeadingAttrs {
  final int level;

  HeadingAttrs(this.level);

  factory HeadingAttrs.fromJson(Map<String, dynamic> json) => _$HeadingAttrsFromJson(json);
}

@JsonSerializable()
class FormulaAttrs {
  final String value;

  FormulaAttrs(this.value);

  factory FormulaAttrs.fromJson(Map<String, dynamic> json) => _$FormulaAttrsFromJson(json);
}

@JsonSerializable()
class ReferenceAttrs {
  @JsonKey(name: 'href')
  final String reference;

  ReferenceAttrs(this.reference);

  factory ReferenceAttrs.fromJson(Map<String, dynamic> json) => _$ReferenceAttrsFromJson(json);
}

@JsonSerializable()
class TableCellAttrs {
  final int colspan;
  final int rowspan;

  TableCellAttrs(this.colspan, this.rowspan);

  factory TableCellAttrs.fromJson(Map<String, dynamic> json) => _$TableCellAttrsFromJson(json);
}
