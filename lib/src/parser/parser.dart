import 'dart:convert' show json;
import '../entities/entities.dart';
import 'package:rich_text/src/parser/models.dart';

class Parser {
  static const _styleMap = {
    'bold': TextNode.BOLD,
    'italic': TextNode.ITALIC,
    'strike': TextNode.STRIKE,
    'underline': TextNode.UNDERLINE,
    'code': TextNode.CODE,
    'link': TextNode.LINK,
  };

  String urlPrefix;
  bool _isCurrentListOrdered = false;
  int _currentListItemOrder = 0;

  Parser(this.urlPrefix);

  Node parseFromString(String jsonString) {
    var map = json.decode(jsonString);
    return parseFromJson(map);
  }

  Node parseFromJson(Map<String, dynamic> json) {
    var root = JsonNode.fromJson(json);
    var preparedRoot = _parseNode(root);
    return preparedRoot;
  }

  Node _parseNode(JsonNode node) {
    switch (node.type) {
      case 'doc':
        var children = _parseNodes(node.content);
        return IntermediateNode(children);
      case 'title':
        var children = _parseNodes(node.content);
        return TitleNode(children);
      case 'paragraph':
        var children = _parseNodes(node.content);
        return ParagraphNode(children);
      case 'heading':
        var attrs = HeadingAttrs.fromJson(node.attrs);
        var children = _parseNodes(node.content);
        return HeadingNode(children, attrs.level);
      case 'list_item':
        var order = _isCurrentListOrdered ? _currentListItemOrder++ : 0;
        var children = _parseNodes(node.content);
        return ListItemNode(children, order);
      case 'bullet_list':
        _isCurrentListOrdered = false;
        var children = _parseNodes(node.content);
        return ListNode(children);
      case 'ordered_list':
        _isCurrentListOrdered = true;
        _currentListItemOrder = 1;
        var children = _parseNodes(node.content);
        return ListNode(children);
      case 'code_block':
        var children = _parseNodes(node.content);
        return CodeBlockNode(children);
      case 'blockquote':
        var children = _parseNodes(node.content);
        return QuoteBlockNode(children);
      case 'image':
        var attrs = ImageAttrs.fromJson(node.attrs);
        var url = (attrs.file != null) ? '$urlPrefix${attrs.file!.url}' : '';
        return ImageNode(url: url, caption: attrs.caption);
      case 'attachment':
        var attrs = AttachmentAttrs.fromJson(node.attrs);
        var url = (attrs.file != null) ? '$urlPrefix${attrs.file!.url}' : '';
        var fileName = attrs.file?.name ?? '';
        var size = attrs.file?.size ?? 0;
        return AttachmentNode(url: url, fileName: fileName, sizeInBytes: size);
      case 'latex':
        var attrs = FormulaAttrs.fromJson(node.attrs);
        return FormulaNode(texCode: attrs.value);
      case 'text':
        int textStyles = 0;
        String? url;
        node.marks?.forEach((it) {
          textStyles |= _styleMap[it.type]!;
          // If current mark is link
          if (textStyles & TextNode.LINK != 0 && (it.attrs?.isNotEmpty ?? false)) {
            var attrs = ReferenceAttrs.fromJson(it.attrs!);
            url = attrs.reference;
            textStyles |= TextNode.UNDERLINE;
          }
        });
        return TextNode(node.text!, styles: textStyles, url: url);
      case 'hard_break':
        return HardBreakNode();
      case 'table':
        var children = _parseNodes(node.content);
        var rows = children.map((node) => node as TableRowNode).toList();
        return TableNode(rows)..prepare();
      case 'table_row':
        var children = _parseNodes(node.content);
        var cells = children.map((node) => node as TableCellNode).toList();
        return TableRowNode(cells);
      case 'table_cell':
        var attrs = TableCellAttrs.fromJson(node.attrs);
        var children = _parseNodes(node.content);
        return TableCellNode(attrs.colspan, attrs.rowspan, children);
    }
    return IntermediateNode([]);
  }

  List<Node> _parseNodes(List<JsonNode>? nodes) {
    List<Node> result = [];
    if (nodes != null) {
      for (var node in nodes) {
        result.add(_parseNode(node));
      }
    }
    return result;
  }
}
