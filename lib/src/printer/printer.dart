import '../entities/entities.dart';

abstract class Printable {
  void print(NodePrinter printer);
}

/// Simple console printer
class NodePrinter {
  int _currentDepth = 0;

  void printIntermediateNode(IntermediateNode node) {
    _printWithIntend('Intermediate');
    _printChildren(node.children);
  }

  void printTitleNode(TitleNode node) {
    _printWithIntend('Title');
    _printChildren(node.children);
  }

  void printParagraphNode(ParagraphNode node) {
    _printWithIntend('Paragraph');
    _printChildren(node.children);
  }

  void printHeadingNode(HeadingNode node) {
    _printWithIntend('Heading [level: ${node.level}]');
    _printChildren(node.children);
  }

  void printListItemNode(ListItemNode node) {
    _printWithIntend('List item [order: ${node.order}]');
    _printChildren(node.children);
  }

  void printListNode(ListNode node) {
    _printWithIntend('List');
    _printChildren(node.children);
  }

  void printCodeBlockNode(CodeBlockNode node) {
    _printWithIntend('Code block');
    _printChildren(node.children);
  }

  void printQuoteBlockNode(QuoteBlockNode node) {
    _printWithIntend('Quote block');
    _printChildren(node.children);
  }

  void printTextLeaf(TextNode node) {
    _printWithIntend('Text [value: ${node.text}, styles: ${node.styles}${node.url != null ? ', url: ' + node.url.toString() : ''}]');
  }

  void printHardBreak(HardBreakNode node) {
    _printWithIntend('Hard break');
  }

  void printImageNode(ImageNode node) {
    _printWithIntend('Image [url: ${node.url}, caption: ${node.caption}]');
  }

  void printAttachmentNode(AttachmentNode node) {
    _printWithIntend('Attachment [url: ${node.url}]');
  }

  void printFormulaNode(FormulaNode node) {
    _printWithIntend('Formula [tex: ${node.texCode}]');
  }

  void printTableNode(TableNode node) {
    _printWithIntend('Table [size: ${node.rowAmount} x ${node.columnAmount}]');
    _printChildren(node.cells);
  }

  void printTableCellNode(TableCellNode node) {
    _printWithIntend('Cell [start: (${node.startRow}, ${node.startColumn}), span: (${node.rowSpan}, ${node.columnSpan})]');
    _printChildren(node.children);
  }

  void _printWithIntend(String text) {
    print('${' ' * (_currentDepth * 2)}$text');
  }

  void _printChildren(List<Node> nodes) {
    for (var child in nodes) {
      _currentDepth++;
      child.print(this);
      _currentDepth--;
    }
  }
}
