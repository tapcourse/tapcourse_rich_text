import 'package:flutter/widgets.dart';
import '../generator/generator.dart';
import '../printer/printer.dart';

abstract class Node implements Printable, Widgetable {}

class IntermediateNode extends Node implements Printable {
  final List<Node> children;

  IntermediateNode(this.children);

  @override
  void print(NodePrinter printer) => printer.printIntermediateNode(this);

  @override
  Widget toWidget(WidgetGenerator generator) => generator.generateIntermediate(this);
}

/// Text block
class TextNode extends Node {
  static const BOLD = 1;
  static const ITALIC = 2;
  static const STRIKE = 4;
  static const UNDERLINE = 8;
  static const CODE = 16;
  static const LINK = 32;

  final String text;
  final int styles;
  final String? url;

  TextNode(
    this.text, {
    this.styles = 0,
    this.url,
  });

  @override
  void print(NodePrinter printer) => printer.printTextLeaf(this);

  @override
  Widget toWidget(WidgetGenerator generator) => generator.generateText(this);
}

/// Line separator
class HardBreakNode extends Node {
  @override
  void print(NodePrinter printer) => printer.printHardBreak(this);

  @override
  Widget toWidget(WidgetGenerator generator) => generator.generateHardBreak(this);
}

/// Image
class ImageNode extends Node {
  final String url;
  final String caption;

  ImageNode({
    required this.url,
    required this.caption,
  });

  @override
  void print(NodePrinter printer) => printer.printImageNode(this);

  @override
  Widget toWidget(WidgetGenerator generator) => generator.generateImage(this);
}

class AttachmentNode extends Node {
  final String url;
  final String fileName;
  final int sizeInBytes;

  AttachmentNode({
    required this.url,
    required this.fileName,
    required this.sizeInBytes,
  });

  @override
  void print(NodePrinter printer) => printer.printAttachmentNode(this);

  @override
  Widget toWidget(WidgetGenerator generator) => generator.generateAttachment(this);
}

/// TeX formula
class FormulaNode extends Node {
  final String texCode;

  FormulaNode({required this.texCode});

  @override
  void print(NodePrinter printer) => printer.printFormulaNode(this);

  @override
  Widget toWidget(WidgetGenerator generator) => generator.generateFormula(this);
}

/// Title text
class TitleNode extends IntermediateNode {
  TitleNode(children) : super(children);

  @override
  void print(NodePrinter printer) => printer.printTitleNode(this);

  @override
  Widget toWidget(WidgetGenerator generator) => generator.generateTitle(this);
}

/// Paragraph
class ParagraphNode extends IntermediateNode {
  ParagraphNode(children) : super(children);

  @override
  void print(NodePrinter printer) => printer.printParagraphNode(this);

  @override
  Widget toWidget(WidgetGenerator generator) => generator.generateParagraph(this);
}

/// Heading text
class HeadingNode extends IntermediateNode {
  final int level;

  HeadingNode(children, this.level) : super(children);

  @override
  void print(NodePrinter printer) => printer.printHeadingNode(this);

  @override
  Widget toWidget(WidgetGenerator generator) => generator.generateHeading(this);
}

/// List
class ListNode extends IntermediateNode {
  ListNode(children) : super(children);

  @override
  void print(NodePrinter printer) => printer.printListNode(this);

  @override
  Widget toWidget(WidgetGenerator generator) => generator.generateList(this);
}

/// List item
class ListItemNode extends IntermediateNode {
  /// if order == 0   - unordered list
  /// if order > 0    - ordered list
  final int order;

  ListItemNode(children, this.order) : super(children);

  @override
  void print(NodePrinter printer) => printer.printListItemNode(this);

  @override
  Widget toWidget(WidgetGenerator generator) => generator.generateListItem(this);
}

/// Code block
class CodeBlockNode extends IntermediateNode {
  CodeBlockNode(children) : super(children);

  @override
  void print(NodePrinter printer) => printer.printCodeBlockNode(this);

  @override
  Widget toWidget(WidgetGenerator generator) => generator.generateCodeBlock(this);
}

/// Quote block
class QuoteBlockNode extends IntermediateNode {
  QuoteBlockNode(children) : super(children);

  @override
  void print(NodePrinter printer) => printer.printQuoteBlockNode(this);

  @override
  Widget toWidget(WidgetGenerator generator) => generator.generateQuoteBlock(this);
}

/// Table
class TableNode extends Node {
  final List<TableRowNode> rows;
  late final int columnAmount;
  late final int rowAmount;
  late final List<TableCellNode> cells;

  TableNode(this.rows);

  void prepare() {
    rowAmount = rows.length;
    columnAmount = rows.first.cells.length;
    cells = [];
    var isPlaced = List.filled(rowAmount * columnAmount, false);
    var currentRow = 0;
    var currentCol = 0;
    for (var row in rows) {
      for (var cell in row.cells) {
        // Searches top-left coordinate for current cell
        while (isPlaced[currentRow * columnAmount + currentCol]) currentCol++;
        // Mark placed table cells
        for (var i = currentRow; i < currentRow + cell.rowSpan; i++) {
          isPlaced.fillRange(i * columnAmount + currentCol, i * columnAmount + currentCol + cell.columnSpan, true);
        }
        cell.startRow = currentRow;
        cell.startColumn = currentCol;
        cells.add(cell);
      }
      currentRow++;
      currentCol = 0;
    }
  }

  @override
  void print(NodePrinter printer) => printer.printTableNode(this);

  @override
  Widget toWidget(WidgetGenerator generator) => generator.generateTable(this);
}

/// Table row
class TableRowNode extends Node {
  final List<TableCellNode> cells;

  TableRowNode(this.cells);

  @override
  void print(NodePrinter printer) {
    // TODO: implement print
  }

  @override
  Widget toWidget(WidgetGenerator generator) => generator.generateTableRow(this);
}

/// Table cell
class TableCellNode extends IntermediateNode {
  final int columnSpan;
  final int rowSpan;
  late final int startRow;
  late final int startColumn;

  TableCellNode(
    this.columnSpan,
    this.rowSpan,
    List<Node> children,
  ) : super(children);

  @override
  void print(NodePrinter printer) => printer.printTableCellNode(this);

  @override
  Widget toWidget(WidgetGenerator generator) => generator.generateTableCell(this);
}
