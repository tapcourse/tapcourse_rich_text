import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter/widgets.dart';
import 'package:rich_text/src/generator/config.dart';
import 'dart:convert' show json;
import 'parser/parser.dart';
import 'generator/generator.dart';
import 'printer/printer.dart';

class TapCourseRichTextGenerator {
  static late NodePrinter _printer;
  static late Parser _parser;
  static late WidgetGenerator _generator;
  static late bool _debug;
  static Duration? _genDelay;

  /// Prepares before parsing and generating rich text widgets
  ///
  /// First of all, you should invoke this function
  static Future<void> initialize({
    String urlPrefix = '',
    RichTextConfig? config,
    bool debug = true,
    Duration? genDelay,
  }) async {
    _debug = debug;
    _genDelay = genDelay;
    _printer = NodePrinter();
    _parser = Parser(urlPrefix);

    late RichTextConfig _config;
    // Uses specified config
    if (config != null) {
      _config = config;
    }
    // Uses default config
    else {
      String jsonString = await rootBundle.loadString('packages/rich_text/assets/config.json');
      var jsonMap = json.decode(jsonString);
      _config = RichTextConfig.fromJson(jsonMap);
    }
    _generator = WidgetGenerator(_config);
  }

  /// Generates a rich text widget tree
  /// 
  /// The passed rich text is represented as a JSON map
  static Future<Widget> generate(Map<String, dynamic> json) async {
    if (json.isEmpty) return SizedBox();
    // Parse json
    var root = _parser.parseFromJson(json);
    // Print parsed json (if debug mode)
    if (_debug) root.print(_printer);
    // Delay (if specified)
    if (_genDelay != null) await Future.delayed(_genDelay!);
    // Generate widget
    return root.toWidget(_generator);
  }
}
